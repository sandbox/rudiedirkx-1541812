<?php

/**
 * Page callback for `submission/%formtype_submission`.
 */
function formtype_submission_page_entity($submission) {
	$content = $submission->buildContent();

	$form = $submission->get_form();

	$variables = compact('content', 'submission', 'form');
	$html = theme('formtype_submission', $variables);

	return $html;
}

/**
 * Page callback for `form/%formtype`.
 */
function formtype_page_entity($form) {
	$content = $form->buildContent();

	$variables = compact('content', 'form');
	$html = theme('formtype_form', $variables);

	return $html;
}

/**
 * Page callback for `admin/structure/formtype`.
 */
function formtype_page_formtypes() {
	$types = _formtype_types();

	// Type format warning (reformat in update formtype_update_3()).
	$any_type = reset($types);
	if (!is_array($any_type)) {
		drupal_set_message(t('You must run !update_link! Formtype Type format is out of date.', array('!update_link' => l('update.php', 'update.php'))), 'warning');
		return '';
	}

	$form_nums = db_query('SELECT type, COUNT(1) num FROM {formtype} GROUP BY type')->fetchAllKeyed(0, 1);

	$items = array();
	foreach ($types as $type => $info) {
		$label = $info['label'];

		$edit_link = l(t('edit'), 'admin/structure/formtype/manage/' . $type);

		$actions = array();

		$actions['fields'] = l(t('Fields'), 'admin/structure/formtype/manage/' . $type . '/fields');

		$actions['display'] = l(t('Display'), 'admin/structure/formtype/manage/' . $type . '/display');

		$num_forms = isset($form_nums[$type]) ? $form_nums[$type] : 0;
		$link_title = t('@num forms', array('@num' => $num_forms));
		$actions['forms'] = l($link_title, 'admin/structure/formtype/manage/' . $type . '/forms');

		$actions['add_form'] = l(t('Add form'), 'admin/structure/formtype/manage/' . $type . '/add');

		$items[] = t($label) . ' (' . $edit_link . ') | ' . implode(' | ', $actions);
	}

	$variables = array('items' => $items, 'title' => 'Formtype types', 'type' => 'ul', 'attributes' => array());
	$html = theme('item_list', $variables);

	return $html;
}

/**
 * Page callback for `admin/structure/formtype/test`.
 */
function formtype_page_test() {
	$info = entity_get_info();
	echo '<pre>';
	print_r($info);
}
