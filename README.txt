
Formtype
====

Create form Types, Forms and Submissions. Forms and Submissions are entities.

To do
----

* [x] Access: `formtype_form_access('view', $form)`
* [x] Access: `formtype_submission_access('create', $submission)`
* [x] `formtype.status` for (un)publishing forms (including entity info alter property)
* [x] Copy fields from existing to new form
* [x] Delete form
* [x] URI `submission/%`
* [x] Submission entity page `extra_fields` (like `uid`, `created` etc)
* [ ] Access overrides (via `formtype_access_OVERRIDE`, infrastructure exists)
* [ ] Delete form type

There are bound to be more necessary Formtype Form options (besides `redirect_uri`
and `submit_message`).
