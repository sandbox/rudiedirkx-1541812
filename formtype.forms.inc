<?php

/**
 * Form callback for `formtype_form_delete_form`.
 */
function formtype_form_delete_form($form, &$form_state, $formtype) {
	$form['#formtype'] = $formtype;

	$question = t('Delete form %title?', array(
		'%title' => $formtype->title,
	));

	$items = array(
		t("Deletion can't be undone."),
		t("More useful stuff about deleting a form..."),
		t("Maybe something about its submissions or fields?"),
	);
	$description = '<ul><li>' . implode("</li>\n<li>", $items) . '</li></ul>';

	$uri = formtype_uri($formtype);
	$path = $uri['path'];

	$form['#submit'][] = 'formtype_form_delete_form_submit';

	return confirm_form($form, $question, $path, $description);
}

/**
 * Submit handler for `formtype_form_delete_form`.
 */
function formtype_form_delete_form_submit($form, &$form_state) {
	$formtype = $form['#formtype'];

	// Delete submissions.
	$query = new EntityFieldQuery();
	$query
		->entityCondition('entity_type', 'formtype_submission')
		->entityCondition('bundle', $formtype->submission_type);
	$result = $query->execute();
	$submissions = array_values(array_map(function($entity) {
		return $entity->ftsid;
	}, $result['formtype_submission']));
	entity_delete_multiple('formtype_submission', $submissions);

	// Submission entity fields?

	// Delete URL aliases.
	

	// Delete entity delete.
	entity_delete('formtype', $formtype->ftid);

	// Clear entity & menu cache.
	entity_info_cache_clear();
	menu_rebuild();

	// Redirect to 'neutral' page.
	$form_state['redirect'] = 'admin/content/formtype/forms';
}

/**
 * Form callback for `formtype_submission_form`.
 */
function formtype_submission_form($form, &$form_state, $submission) {
	$form_state['submission'] = $submission;
	$is_new = $submission->is_new;

	// Add field widgets.
	field_attach_form('formtype_submission', $submission, $form, $form_state);

	$form['actions'] = array('#type' => 'actions');
	$form['actions']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Submit'),
	);

	$form['#validate'][] = 'formtype_submission_form_validate';
	$form['#submit'][] = 'formtype_submission_form_submit';

	return $form;
}

/**
 * Validation handler for `formtype_submission_form`.
 */
function formtype_submission_form_validate($form, &$form_state) {
	$submission = $form_state['submission'];
	$is_new = $submission->is_new;
	$values = &$form_state['values'];

	// Validate field widgets.
	field_attach_form_validate('formtype_submission', $submission, $form, $form_state);
}

/**
 * Submit handler for `formtype_submission_form`.
 */
function formtype_submission_form_submit($form, &$form_state) {
	$submission = $form_state['submission'];
	$is_new = $submission->is_new;
	$values = &$form_state['values'];
	$caller = __FUNCTION__;

	global $user;

	if ($is_new) {
		$submission->created = REQUEST_TIME;
		$submission->ip = $_SERVER['REMOTE_ADDR'];
		if ($user->uid) {
			$submission->uid = $user->uid;
		}
	}

	// Submit field widgets.
	field_attach_submit('formtype_submission', $submission, $form, $form_state);

	// Call `presubmit` alters.
	drupal_alter('formtype_submission_presubmit', $form_state, $caller);

	// Save form.
	$submission->save();

	// Prepare `postsubmit` values.
	$form_state['message'] = t('Submission entity saved.');
	$form_state['redirect_uri'] = $submission->form->redirect_uri ?: '<front>';

	// Call `postsubmit` alters.
	drupal_alter('formtype_submission_postsubmit', $form_state, $caller);

	if ($form_state['message']) {
		drupal_set_message($form_state['message']);
	}

	if ($form_state['redirect_uri']) {
		$form_state['redirect'] = $form_state['redirect_uri'];
	}
}

/**
 * Form callback for `formtype_form_form`.
 */
function formtype_form_form($form, &$form_state, $formtype) {
	if (!is_a($formtype, 'FormTypeForm')) {
		$formtype = entity_get_controller('formtype')->create(array(
			'type' => $formtype->type,
		));
	}
	$is_new = $formtype->is_new;
	$form_state['formtype'] = $formtype;


	// Help text.
	if (!$is_new) {
		$link = l(t('You can edit its <strong>submission entity</strong> fields here.'), 'admin/structure/formtype_submission/manage/' . $formtype->submission_type . '/fields', array('html' => 1));
		$part_1 = '<p>' . t("You're editing the <strong>form entity</strong>. !link", array('!link' => $link)) . '</p>';

		$form['form_description'] = array(
			'#type' => 'markup',
			'#markup' => $part_1,
			'#weight' => -21,
		);
	}


	// Form entity fields.
	$form['entity'] = array(
		'#type' => 'fieldset',
		'#title' => t('Form entity'),
	);
	if (!$is_new) {
		$link = l(t('Edit the fields below here.'), 'admin/structure/formtype/manage/' . $formtype->type . '/fields', array('html' => 1));
		$part_2 = '<p>' . t("This form is of type %type. !link", array('%type' => $formtype->type, '!link' => $link)) . '</p>';

		$form['entity']['#description'] = $part_2;
	}

	$form['entity']['title'] = array(
		'#type' => 'textfield',
		'#title' => t('Title'),
		'#default_value' => $formtype->title,
		'#required' => TRUE,
		'#weight' => -11,
	);

	if ($is_new) {
		$form['entity']['machine_name'] = array(
			'#type' => 'machine_name',
			'#default_value' => '',
			'#maxlength' => 60,
			'#machine_name' => array(
				'exists' => 'formtype_machine_name_load',
				'source' => array('entity', 'title'),
				'label' => t('Machine-readable name'),
				'replace_pattern' => '[^a-z0-9_]+',
				'replace' => '_',
			),
			'#weight' => -10,
		);
	}
	else {
		$form['entity']['title']['#field_suffix'] = ' ' . t('Machine-readable name: %name', array('%name' => $formtype->submission_type));
	}


	// Add field widgets.
	field_attach_form('formtype', $formtype, $form['entity'], $form_state);


	// Meta settings.
	$form['meta'] = array(
		'#type' => 'fieldset',
		'#title' => t('Entity meta settings'),
		'#collapsible' => TRUE,
		'#group' => 'additional_settings',
	);

	$form['meta']['status'] = array(
		'#type' => 'checkbox',
		'#title' => t('Published'),
		'#default_value' => $formtype->status,
		'#description' => t('Like <code>node.status</code>. Disabled forms will not be accessible to users without administer access.'),
	);


	// Submission settings (fields).
	$form['submission'] = array(
		'#type' => 'fieldset',
		'#title' => t('Submission settings'),
		'#collapsible' => TRUE,
		'#group' => 'additional_settings',
	);

	$form['submission']['redirect_uri'] = array(
		'#type' => 'textfield',
		'#title' => t('Redirect URI'),
		'#default_value' => $formtype->redirect_uri,
		'#description' => t('If you need something advanced, you can override this in <code>@hook</code>.', array('@hook' => 'hook_formtype_submission_postsubmit_alter')),
	);

	$form['submission']['submit_message'] = array(
		'#type' => 'textfield',
		'#title' => t('Submit message'),
		'#default_value' => $formtype->submit_message,
		'#description' => t('If you need something advanced, you can override this in <code>@hook</code>.', array('@hook' => 'hook_formtype_submission_postsubmit_alter')),
	);

	$permissions_link = l(t('Permissions page'), 'admin/people/permissions', array('fragment' => 'module-formtype'));
	$simple_permissions = t('Permissions, like who can submit and how often, can be set on the !permissions_link.', array('!permissions_link' => $permissions_link));
	$advanced_permissions = t('If you need something advanced, you can override this in <code>@hook</code>.', array('@hook' => 'hook_formtype_form_access'));
	$form['submission']['permissions'] = array(
		'#type' => 'item',
		'#title' => t('Permissions'),
		'#markup' => $simple_permissions . ' <strike>' . $advanced_permissions . '</strike>',
	);


	// Path/URL aliases.
	if (module_exists('path')) {
		$source = $is_new ? NULL : 'form/' . $formtype->ftid;
		$path = array();
		if (!$is_new) {
			$path = path_load($source) ?: array();
		}
		$path += array(
			'pid' => NULL,
			'source' => $source,
			'alias' => '',
			'language' => LANGUAGE_NONE,
		);

		$form['path'] = array(
			'#type' => 'fieldset',
			'#title' => t('URL path settings'),
			'#collapsible' => TRUE,
			'#collapsed' => FALSE,
			'#group' => 'additional_settings',
			'#attributes' => array(
				'class' => array('path-form'),
			),
			'#attached' => array(
				'js' => array(drupal_get_path('module', 'path') . '/path.js'),
			),
			'#access' => user_access('create url aliases') || user_access('administer url aliases'),
			'#weight' => 30,
			'#tree' => TRUE,
			'#element_validate' => array('path_form_element_validate'),
		);

		$form['path']['alias'] = array(
			'#type' => 'textfield',
			'#title' => t('URL alias'),
			'#default_value' => $path['alias'],
			'#maxlength' => 255,
			'#description' => t('Optionally specify an alternative URL by which this content can be accessed. For example, type "about" when writing an about page. Use a relative path and don\'t add a trailing slash or the URL alias won\'t work.'),
		);
		$form['path']['pid'] = array('#type' => 'value', '#value' => $path['pid']);
		$form['path']['source'] = array('#type' => 'value', '#value' => $path['source']);
		$form['path']['language'] = array('#type' => 'value', '#value' => $path['language']);
	}


	// Copy fields from existing form (only for new forms).
	if ($is_new) {
		$open = !empty($form_state['input']['fields_source']);

		$form['copy'] = array(
			'#type' => 'fieldset',
			'#title' => t('Copy fields'),
			'#collapsible' => TRUE,
			'#collapsed' => !$open,
		);
		$forms = db_select('formtype', 'ft')
			->fields('ft', array('ftid', 'title'))
			->orderBy('title')
			->execute()
			->fetchAllKeyed(0, 1);
		$form['copy']['fields_source'] = array(
			'#type' => 'select',
			'#title' => t('Copy fields from'),
			'#options' => $forms,
			'#empty_option' => t('- None -'),
			'#description' => t('You can copy the fields from an existing form into this new one.'),
			'#weight' => 10,
		);
	}


    $form['additional_settings'] = array(
      '#type' => 'vertical_tabs',
      '#weight' => 99,
    );


	$form['actions'] = array('#type' => 'actions');
	$form['actions']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Submit'),
	);
	$form['actions']['delete'] = array(
		'#type' => 'link',
		'#title' => t('Delete'),
		'#href' => 'form/' . $formtype->ftid . '/delete',
	);


	$form['#validate'][] = 'formtype_form_form_validate';
	$form['#submit'][] = 'formtype_form_form_submit';

	return $form;
}

/**
 * Validation handler for `formtype_form_form`.
 */
function formtype_form_form_validate($form, &$form_state) {
	$formtype = $form_state['formtype'];
	$is_new = $formtype->is_new;
	$values = &$form_state['values'];

	// Validate field widgets.
	field_attach_form_validate('formtype', $formtype, $form, $form_state);
}

/**
 * Submit handler for `formtype_form_form`.
 */
function formtype_form_form_submit($form, &$form_state) {
	$formtype = $form_state['formtype'];
	$is_new = $formtype->is_new;
	$values = &$form_state['values'];

	global $user;

	// Fetch old URL path alias
	if (module_exists('path')) {
		if (!$is_new) {
			$path = path_load('form/' . $formtype->ftid);
		}
	}

	// Add custom fields.
	$formtype->title = $values['title'];

	if ($is_new) {
		$formtype->submission_type = $values['machine_name'];
		$formtype->created = REQUEST_TIME;
		$formtype->uid = $user->uid;
	}

	$formtype->status = $values['status'];
	$formtype->redirect_uri = $values['redirect_uri'];

	// Submit field widgets.
	field_attach_submit('formtype', $formtype, $form, $form_state);

	// Save form.
	$formtype->save();

	// Save new URL path alias.
	if (module_exists('path')) {
		if (empty($path) || $path['alias'] != $values['path']['alias']) {
			if (isset($path['pid']) && empty($values['path']['alias'])) {
				path_delete((int) $path['pid']);
			}
			else if (!empty($values['path']['alias'])) {
				path_save($values['path']);
			}
		}
	}

	if ($is_new) {
		// Clear entity cache.
		entity_info_cache_clear();
		menu_rebuild();

		// Copy fields from `fields_source`.
		if ($values['fields_source']) {
			$source_formtype = formtype_load($values['fields_source']);
			$info = _field_info_collate_fields();
			$field_instances = $info['instances']['formtype_submission'][$source_formtype->submission_type];
			foreach ($field_instances as $field_instance) {
				$field_instance['bundle'] = $formtype->submission_type;
				watchdog('field_create_instance', print_r(field_create_instance($field_instance), 1));
			}

			entity_info_cache_clear();
			menu_rebuild();
		}

		// Submission entity manage fields URI.
		$uri = 'admin/structure/formtype_submission/manage/' . $formtype->submission_type . '/fields';
	}
	else {
		// Form entity URI.
		$uri = formtype_uri($formtype);
		$uri = $uri['path'];
	}

	$form_state['redirect'] = $uri;
}

/**
 * Form callback for `formtype_type_form`.
 */
function formtype_type_form($form, &$form_state, $type = NULL) {
	$form_state['formtype']['type'] = $type;
	$is_new = empty($form_state['formtype']['type']);

	$form['label'] = array(
		'#type' => 'textfield',
		'#title' => t('Label'),
		'#default_value' => isset($type->label) ? $type->label : '',
		'#required' => TRUE,
	);

	if ($is_new) {
		$form['machine_name'] = array(
			'#type' => 'machine_name',
			'#default_value' => '',
			'#maxlength' => 60,
			'#machine_name' => array(
				'exists' => 'formtype_type_load',
				'source' => array('label'),
				'label' => t('Machine name'),
				'replace_pattern' => '[^a-z0-9_]+',
				'replace' => '_',
			),
		);
	}
	else {
		$form['label']['#field_suffix'] = ' ' . t('Machine-readable name: %name', array('%name' => $type->type));
		$form['machine_name'] = array(
			'#type' => 'value',
			'#value' => $type->type,
		);
	}

	$form['actions'] = array('#type' => 'actions');
	$form['actions']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Submit'),
	);

	$form['#submit'][] = 'formtype_type_form_submit';

	return $form;
}

/**
 * Submit handler for `formtype_type_form`.
 */
function formtype_type_form_submit($form, &$form_state) {
	$type = $form_state['formtype']['type'];
	$is_new = empty($type);

	$values = &$form_state['values'];

	// Existing types.
	$types = _formtype_types();

	// Extend with new (or changed).
	$types[$values['machine_name']] = array(
		'label' => $values['label'],
	);

	// Save all into 1 variable.
	variable_set('formtype_types', $types);

	drupal_set_message(t('Formtype Type saved.'));
	$form_state['redirect'] = 'admin/structure/formtype';
}
