<?php

/**
 * Implements hook_entity_info().
 */
function formtype_entity_info() {
	return array(
		'formtype' => array(
			'module' => 'formtype',
			'label' => t('Formtype form'),
			'controller class' => 'FormTypeFormAPIController',
			'entity class' => 'FormTypeForm',
			'base table' => 'formtype',
			'fieldable' => TRUE,
			'entity keys' => array(
				'id' => 'ftid',
				'bundle' => 'type',
			),
			'bundle keys' => array(
				'bundle' => 'type',
			),
			'bundles' => array(),
			'view modes' => array(
				'full' => array(
					'label' => t('Full'),
					'custom settings' => FALSE,
				),
			),
			'uri callback' => 'formtype_uri',
			'token type' => 'formtype',
		),
		'formtype_submission' => array(
			'module' => 'formtype',
			'label' => t('Formtype submission'),
			'controller class' => 'FormTypeSubmissionAPIController',
			'entity class' => 'FormTypeSubmission',
			'base table' => 'formtype_submission',
			'fieldable' => TRUE,
			'entity keys' => array(
				'id' => 'ftsid',
				'bundle' => 'type',
			),
			'bundle keys' => array(
				'bundle' => 'type',
			),
			'bundles' => array(),
			'view modes' => array(
				'full' => array(
					'label' => t('Full'),
					'custom settings' => FALSE,
				),
			),
			'uri callback' => 'formtype_submission_uri',
			'token type' => 'formtype_submission',
		),
	);
}

/**
 * Implements hook_entity_info_alter().
 */
function formtype_entity_info_alter(&$info) {
	// Bundles for `formtype`.
	foreach (_formtype_types() as $type => $type_info) {
		$label = $type_info['label'];
		$info['formtype']['bundles'][$type] = array(
			'label' => $label,
			'name' => $label,
			'type' => $type,
			'admin' => array(
				'path' => 'admin/structure/formtype/manage/%formtype_type',
				'real path' => 'admin/structure/formtype/manage/' . $type,
				'bundle argument' => 4,
				'access arguments' => array('administer formtype forms'),
			),
		);
	}

	// Bundles for `formtype_submission`.
	$forms = db_select('formtype', 'ft')
		->fields('ft', array('submission_type', 'title'))
		->execute();
	foreach ($forms as $form) {
		$type = $form->submission_type;
		$label = $form->title;
		$info['formtype_submission']['bundles'][$type] = array(
			'label' => $label,
			'name' => $label,
			'type' => $type,
			'admin' => array(
				'path' => 'admin/structure/formtype_submission/manage/%formtype_submission_type',
				'real path' => 'admin/structure/formtype_submission/manage/' . $type,
				'bundle argument' => 4,
				'access arguments' => array('administer formtype submissions'),
			),
		);
	}
}

/**
 * Implements hook_entity_property_info_alter().
 */
function formtype_entity_property_info_alter(&$info) {
	// Form properties.
	$info['formtype']['properties']['user'] = array(
		'type' => 'user',
		'label' => 'Author',
		'description' => 'The creator of this Form.',
	) + $info['formtype']['properties']['uid'];

	$info['formtype']['properties']['status']['type'] = 'boolean';

	// Submission properties.
	$info['formtype_submission']['properties']['form'] = array(
		'type' => 'formtype',
		'label' => 'Form',
		'description' => 'The related Form entity.',
	) + $info['formtype_submission']['properties']['ftid'];

	$info['formtype_submission']['properties']['user'] = array(
		'type' => 'user',
		'label' => 'Author',
		'description' => 'The author of this submission. Can be NULL (Anonymous).',
	) + $info['formtype_submission']['properties']['uid'];

	$info['formtype_submission']['properties']['created']['type'] = 'date';
}

/**
 * Implements hook_field_extra_fields().
 */
function formtype_field_extra_fields() {
	foreach (_formtype_types() as $type => $type_info) {
		$fields['formtype'][$type]['form']['title'] = array(
			'label' => t('Title'),
			'description' => t('Title'),
			'weight' => -10,
		);
		$fields['formtype'][$type]['display']['formtype_form'] = array(
			'label' => t('Form'),
			'description' => t('Form'),
			'weight' => 10,
		);
	}

	foreach (_formtype_submission_types() as $type => $title) {
		$fields['formtype_submission'][$type]['display']['type'] = array(
			'label' => t('Type'),
			'description' => t('Type'),
			'weight' => -10,
		);
		$fields['formtype_submission'][$type]['display']['uid'] = array(
			'label' => t('User'),
			'description' => t('User'),
			'weight' => -10,
		);
		$fields['formtype_submission'][$type]['display']['created'] = array(
			'label' => t('Created'),
			'description' => t('Created'),
			'weight' => -10,
		);
		$fields['formtype_submission'][$type]['display']['ip'] = array(
			'label' => t('IP'),
			'description' => t('IP'),
			'weight' => -10,
		);
	}

	return $fields;
}

/**
 * Implements hook_entity_view().
 */
function formtype_entity_view($entity, $entity_type, $view_mode, $langcode) {
	if ('formtype' == $entity_type) {
		$submission = entity_get_controller('formtype_submission')->create(array(
			'ftid' => $entity->ftid,
			'type' => $entity->submission_type,
		));

		$access = formtype_form_access('submit', $entity);

		module_load_include('inc', 'formtype', 'formtype.forms');
		$form = drupal_get_form('formtype_submission_form', $submission);

		$entity->content['formtype_form'] = array(
			'#access' => $access,
			'#type' => 'container',
			'#attributes' => array('class' => array('field', 'field-name-form', 'field-type-form')),
			'form' => $form,
		);
	}
	elseif ('formtype_submission' == $entity_type) {
		$formtype = formtype_load($entity->type);
		$uri = formtype_uri($formtype);

		$entity->content['type'] = array(
			'#type' => 'item',
			'#title' => t('Type'),
			'#attributes' => array('class' => array('field', 'field-name-type', 'field-type-item')),
			'#markup' => l($formtype->title, $uri['path'], $uri),
		);

		$entity->content['uid'] = array(
			'#type' => 'item',
			'#title' => t('User'),
			'#attributes' => array('class' => array('field', 'field-name-uid', 'field-type-item')),
			'#markup' => $entity->uid,
		);

		$entity->content['created'] = array(
			'#type' => 'item',
			'#title' => t('Created'),
			'#attributes' => array('class' => array('field', 'field-name-created', 'field-type-item')),
			'#markup' => format_date($entity->created),
		);

		$entity->content['ip'] = array(
			'#type' => 'item',
			'#title' => t('IP'),
			'#attributes' => array('class' => array('field', 'field-name-ip', 'field-type-item')),
			'#markup' => $entity->ip,
		);
	}
}

/**
 * Loader for `formtype_submission`.
 */
function formtype_submission_load($ftsid) {
	$entities = entity_load('formtype_submission', array($ftsid), array(), FALSE);
	return reset($entities) ?: FALSE;
}

/**
 * Loader for `formtype`.
 */
function formtype_load($ftid) {
	if ((string) $ftid != (string) (int) $ftid) {
		return formtype_machine_name_load($ftid);
	}

	$entities = entity_load('formtype', array($ftid), array(), FALSE);
	return reset($entities) ?: FALSE;
}

/**
 * Loader for `formtype_machine_name`.
 */
function formtype_machine_name_load($machine_name) {
	$entities = entity_load('formtype', FALSE, array(
		'submission_type' => $machine_name,
	), FALSE);

	return reset($entities) ?: FALSE;
}

/**
 * Loader for `formtype_type`.
 */
function formtype_type_load($type) {
	$types = _formtype_types();

	if (isset($types[$type])) {
		$type_info = $types[$type];
		$type_info['type'] = $type;
		return (object) $type_info;
	}

	return FALSE;
}

/**
 * Loader for `formtype_submission_type`.
 */
function formtype_submission_type_load($type) {
	return (object) array(
		'label' => $type,
		'type' => $type,
	);
}

/**
 * URI callback for `formtype`.
 */
function formtype_uri($entity) {
	return array(
		'path' => 'form/' . $entity->ftid,
		//'query' => array('type' => $entity->type),
	);
}

/**
 * URI callback for `formtype_submission`.
 */
function formtype_submission_uri() {
	return array(
		'path' => 'submission/' . $entity->ftsid,
	);
}

/**
 * Entity classes
 */

abstract class FormTypeEntity extends Entity {
	public $is_new = false;

	public function buildContent($view_mode = 'full', $langcode = NULL) {
		$info = $this->entityInfo();
		$primary_key = $info['entity keys']['id'];
		$entities = array($this->$primary_key => $this);

		field_attach_prepare_view($this->entity_type, $entities, $view_mode, $langcode);
		entity_prepare_view($this->entity_type, $entities, $langcode);

		$content = parent::buildContent($view_mode, $langcode);
		$content += array('#view_mode' => $view_mode);

		return $content;
	}
}

class FormTypeFormAPIController extends EntityAPIController {}

class FormTypeForm extends FormTypeEntity {
	protected $entity_type = 'formtype';

	public $ftid = 0;
	public $status = 1;
	public $title = '';
	public $redirect_uri = '';
	public $submit_message = '';

	public function submittedBy($uid) {
		return db_query('SELECT COUNT(1) FROM {formtype_submission} WHERE ftid = ? AND uid = ?', array(
			$this->ftid,
			$uid
		))->fetchField();
	}
}

class FormTypeSubmissionAPIController extends EntityAPIController {
	public function create(array $values = array()) {
		$values['form'] = formtype_load($values['ftid']);
		return parent::create($values);
	}
}

class FormTypeSubmission extends FormTypeEntity {
	protected $entity_type = 'formtype_submission';

	public $ftsid = 0;
	public $ftid = 0;

	public function get_form() {
		if (!isset($this->form)) {
			$this->form = formtype_load($this->ftid);
		}

		return $this->form;
	}
}
